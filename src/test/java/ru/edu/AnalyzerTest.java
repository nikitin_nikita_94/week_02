package ru.edu;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AnalyzerTest {

    /**
     * По умолчанию задачи запускаются с рабочей директорией в корне проекта
     */
    public static final String FILE_PATH = "./src/test/resources/input_text.txt";
    public static final String FILE_PATH_REPORTER = "./result.txt";
    public static final int EXPECTED_WORDS = 547;
    public static final int EXPECTED_CHARS = 3396;
    public static final int EXPECTED_CHARS_WO_SPACES = 2941;
    public static final int EXPECTED_CHARS_ONLY_PUNCT = 160;

    /**
     * Нужно переключиться на вашу реализацию
     */
    private final TextAnalyzer analyzer = new TextAnalyzerImpl();

    /**
     * Нужно переключиться на вашу реализацию
     */
    private final SourceReader reader = new SourceReaderImpl();

    private final StatisticReporter reporter = new StatisticReporterImpl(FILE_PATH_REPORTER);


    @Test
    public void validation() {
        reader.setup(FILE_PATH);
        TextStatistics statistics = reader.readSource(analyzer);

        assertNotNull(statistics);

        assertEquals(EXPECTED_WORDS, statistics.getWordsCount());
        assertEquals(EXPECTED_CHARS, statistics.getCharsCount());
        assertEquals(EXPECTED_CHARS_WO_SPACES, statistics.getCharsCountWithoutSpaces());
        assertEquals(EXPECTED_CHARS_ONLY_PUNCT, statistics.getCharsCountOnlyPunctuations());
        reporter.report(statistics);
        System.out.println(statistics);

        /*List<String> topWords = statistics.getTopWords();

        assertEquals(0, topWords.size());*/
    }
}
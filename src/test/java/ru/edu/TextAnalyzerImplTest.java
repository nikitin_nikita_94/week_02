package ru.edu;

import org.junit.Test;

import static org.junit.Assert.*;

public class TextAnalyzerImplTest {

    @Test
    public void analyze() {
        TextAnalyzer analyzer = new TextAnalyzerImpl();

        analyzer.analyze("First line test!");

        TextStatistics statistics = analyzer.getStatistic();

        assertEquals(3,statistics.getWordsCount());
        assertEquals(16,statistics.getCharsCount());
        assertEquals(14,statistics.getCharsCountWithoutSpaces());
        assertEquals(1,statistics.getCharsCountOnlyPunctuations());
    }
}
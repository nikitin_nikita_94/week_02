package ru.edu;

import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import static org.junit.Assert.*;

public class StatisticReporterImplTest {

    public static final String RESULT_TEXT_REPORTER = "./target/TextStatisticReporter.txt";

    @Test
    public void reportTest() {
        StatisticReporter reporter = new StatisticReporterImpl(RESULT_TEXT_REPORTER);

        TextStatistics statistics = new TextStatistics();

        statistics.addWords(1);
        statistics.addChars(2);
        statistics.addCharsWithoutSpaces(3);
        statistics.addCharsOnlyPunctuations(4);

        assertNotNull(statistics);

        reporter.report(statistics);

        String textReporter = fileReaderTest();
        assertEquals(
        "TextStatistics" +
                "{wordsCount=1," +
                " charsCount=2," +
                " charsCountWithoutSpaces=3," +
                " charsCountOnlyPunctuations=4}", textReporter);
    }

    private String fileReaderTest() {
        StringBuilder sb = new StringBuilder();

        try (BufferedReader br = new BufferedReader(new FileReader(RESULT_TEXT_REPORTER))) {
            sb.append(br.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return String.valueOf(sb);
    }
}
package ru.edu;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class SourceReaderImplTest {

    public static final String FILE_PATH_TEST = "./target/FILE_PATH_TEST.txt";
    public static final String NOT_EXIST_FILE_PATH_TEST = "./target/Test.txt";

    SourceReader reader = new SourceReaderImpl();

    @Test
    public void setupExistFilePathTest() {
        reader.setup(FILE_PATH_TEST);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setupNullTest() {
        reader.setup(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setupNotExistFilePathTest() {
        reader.setup(NOT_EXIST_FILE_PATH_TEST);
    }

    @Test
    public void readSource() {
        TestAnalyzerText analyzerText = new TestAnalyzerText();
        reader.setup(FILE_PATH_TEST);
        TextStatistics statistics = reader.readSource(analyzerText);

        assertNotNull(statistics);
        assertEquals(2, statistics.getWordsCount());
        assertEquals("one line test class SourceReader,", analyzerText.list.get(0));
        assertEquals("two, line test class SourceReader.", analyzerText.list.get(1));
    }

    public static class TestAnalyzerText implements TextAnalyzer {

        List<String> list = new ArrayList<>();

        /**
         * Анализ строки произведения.
         *
         * @param line
         */
        @Override
        public void analyze(String line) {
            list.add(line);
        }

        /**
         * Получение сохраненной статистики.
         *
         * @return TextStatistic
         */
        @Override
        public TextStatistics getStatistic() {
            TextStatistics statistics = new TextStatistics();
            statistics.addWords(list.size());
            return statistics;
        }
    }
}
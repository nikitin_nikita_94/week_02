package ru.edu;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        AnalyzerTest.class,
        SourceReaderImplTest.class,
        StatisticReporterImplTest.class,
        TextAnalyzerImplTest.class
})
public class AllTests {
}

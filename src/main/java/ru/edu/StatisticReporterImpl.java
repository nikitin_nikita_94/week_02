package ru.edu;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class StatisticReporterImpl implements StatisticReporter {

    /**
     * Обьект который будет содержать путь до файла.
     */
    private File path;

    /**
     * Пулучение пути до файла.
     *
     * @param path
     */
    public StatisticReporterImpl(String path) {
        this.path = new File(path);
    }

    /**
     * Формирование отчета.
     *
     * @param statistics - данные статистики
     */
    @Override
    public void report(final TextStatistics statistics) {
        try (BufferedWriter bW = new BufferedWriter(new FileWriter(path))) {
            bW.write(statistics.toString());
        } catch (IOException e) {
            System.out.println(
                    "There was an error read the message carefully"
                            + e.getMessage());
        }
    }
}

package ru.edu;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class SourceReaderImpl implements SourceReader {
    /**
     * Путь до файла.
     */
    private File file;

    /**
     * Установка источника.
     * В реализации тут будет приходить путь до файла-источника
     *
     * @param source
     */
    @Override
    public void setup(final String source) {
        if (source == null) {
            throw new IllegalArgumentException("line is exactly null");
        }

        file = new File(source);
        if (!file.exists()) {
            throw new IllegalArgumentException("File not found");
        }
    }

    /**
     * Метод для анализа источника.
     *
     * @param analyzer - логика подсчета статистики
     * @return - рассчитанная статистика
     */
    @Override
    public TextStatistics readSource(final TextAnalyzer analyzer) {
        return read(analyzer);
    }

    private TextStatistics read(final TextAnalyzer analyzer) {
        if (analyzer == null) {
            throw new IllegalArgumentException("Analyzer is NULL");
        }

        try (BufferedReader bf = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = bf.readLine()) != null) {
                analyzer.analyze(line);
            }
        } catch (IOException e) {
            throw new RuntimeException(
                    "There was an error read the message carefully"
                    + e.getMessage());
        }

        return analyzer.getStatistic();
    }
}

package ru.edu;

/**
 *
 * Интерфейс для чтения источника.
 * Требуется реализация SourceReaderImpl, в котором
 * будет читаться содержимое файла.
 *
 * Реализации данного интерфейса отвечают только за чтение, подсчет будет
 * делегироваться в TextAnalyzer
 */
public interface SourceReader {

    /**
     * Установка источника.
     * В реализации тут будет приходить путь до файла-источника
     *
     * @param source
     */
    void setup(String source);

    /**
     * Метод для анализа источника.
     *
     * @param analyzer - логика подсчета статистики
     * @return - рассчитанная статистика
     */
    TextStatistics readSource(TextAnalyzer analyzer);
}

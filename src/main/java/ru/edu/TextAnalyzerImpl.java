package ru.edu;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TextAnalyzerImpl implements TextAnalyzer {
    /**
     * Обьект где хранятся изменения.
     */
    private TextStatistics textStat = new TextStatistics();

    /**
     * Анализ строки произведения.
     *
     * @param line
     */
    @Override
    public void analyze(final String line) {
        textStat.addWords(line.split("[[\\d\\s]+]+").length);
        textStat.addChars(line.length());
        textStat.addCharsWithoutSpaces(line.replaceAll("\\s+", "").length());
        textStat.addCharsOnlyPunctuations(line.split("[\\p{P}\\p{S}]").length);
    }

    /**
     * Получение сохраненной статистики.
     *
     * @return TextStatistic
     */
    @Override
    public TextStatistics getStatistic() {
        return textStat;
    }
}

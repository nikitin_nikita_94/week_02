package ru.edu;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Необходимо реализовать методы модификации
 * и доступа к хранимым приватным переменным.
 */
public class TextStatistics {

    /**
     * Всего слов.
     */
    private long wordsCount;

    /**
     * Всего символов.
     */
    private long charsCount;

    /**
     * Всего символов без пробелов.
     */
    private long charsCountWithoutSpaces;

    /**
     * Всего знаков препинания.
     */
    private long charsCountOnlyPunctuations;


    /**
     * Получение количества слов.
     *
     * @return значение
     */
    public long getWordsCount() {
        return wordsCount;
    }

    /**
     * Добавление количества слов.
     *
     * @param words
     */
    public void addWords(final long words) {
        wordsCount += words;
    }

    /**
     * Получение количества символов.
     *
     * @return значение
     */
    public long getCharsCount() {
        return charsCount;
    }

    /**
     * Добавление количества символов.
     *
     * @param chars
     */
    public void addChars(final long chars) {
        charsCount += chars;
    }

    /**
     * Получение количества слов без пробелов.
     *
     * @return значение
     */
    public long getCharsCountWithoutSpaces() {
        return charsCountWithoutSpaces;
    }

    /**
     * Добавление количества слов без пробелов.
     *
     * @param charsWithoutSpaces
     */
    public void addCharsWithoutSpaces(final long charsWithoutSpaces) {
        charsCountWithoutSpaces += charsWithoutSpaces;
    }

    /**
     * Получение количества знаков препинания.
     *
     * @return значение
     */
    public long getCharsCountOnlyPunctuations() {
        return charsCountOnlyPunctuations;
    }

    /**
     * Добавление количества знаков препинания.
     *
     * @param charsOnlyPunctuations
     */
    public void addCharsOnlyPunctuations(final long charsOnlyPunctuations) {
        charsCountOnlyPunctuations += charsOnlyPunctuations;
    }

    /**
     * Задание со звездочкой.
     * Необходимо реализовать нахождение топ-10 слов.
     *
     * @return List из 10 популярных слов
     */
    public List<String> getTopWords() {
        return Collections.emptyList();
    }

    /**
     * Текстовое представление.
     *
     * @return текст
     */
    @Override
    public String toString() {
        return "TextStatistics{"
                + "wordsCount=" + wordsCount
                + ", charsCount=" + charsCount
                + ", charsCountWithoutSpaces=" + charsCountWithoutSpaces
                + ", charsCountOnlyPunctuations=" + charsCountOnlyPunctuations
                + '}';
    }
}
